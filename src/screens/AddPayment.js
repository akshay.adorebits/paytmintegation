import React from 'react';
import { TouchableHighlight, Alert, View, Text, StyleSheet, Dimensions, NativeModules, ActivityIndicator, DeviceEventEmitter, TextInput , Modal,SafeAreaView} from 'react-native';
import { WebView } from 'react-native-webview';
//import Paytm from '@philly25/react-native-paytm';
var {height, width} = Dimensions.get('window');

export default class AddPayment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal : false,
            ack : "",
            ORDER_ID : 0,
            TXN_AMOUNT : "",
            CUST_ID : "",
            amount: ""
        };
       // this._handleResponse = this._handleResponse.bind(this);
    }

    _startPayment(){
        var order_id = Math.floor(1000 + Math.random() * (99999 - 10000));
        if(this.state.amount !== ''){
            this.setState({
                showModal:true,
                ORDER_ID: order_id,
                TXN_AMOUNT : this.state.amount,
                CUST_ID : "Akshay_123"
            })
        }else{
            Alert.alert("Please enter amount");
        }
    }

    _handleResponse = (title) => {
        if(title == 'true'){
            this.setState({showModal:false, ack:"Transaction successfull complete "});
            this.setState({amount:"",TXN_AMOUNT:""})
            Alert.alert("Transaction successfull complete")
        }else if(title == 'false'){
            this.setState({showModal: false, ack:"Oops ! Somethig wrong ",amount:""})
            this.setState({amount:"",TXN_AMOUNT:""})
            Alert.alert("Oops ! Somethig wrong ")
        }else {
            return
        }
    }
    render() {
        //return <WebView source={{ uri: 'https://reactnative.dev/' }} />;
        let {ORDER_ID,ack,TXN_AMOUNT,CUST_ID,showModal,amount} = this.state;
        return (
            <SafeAreaView style={{flex:1}}>
                <TextInput 
                    style={{height: 50, backgroundColor: '#EDEDED', color: 'black', marginLeft: 10, marginRight:10, marginTop: 20,paddingHorizontal:10}}
                    onChangeText={(amount) => this.setState({amount})}
                    autoCorrect={false}
                    placeholder=" Enter Amount"
                    placeholderTextColor="#727278"
                    keyboardType="numeric"
                />
                <TouchableHighlight onPress={()=>this._startPayment()}
                    underlayColor='transparent' style={{marginTop: 16, alignItems:'center', justifyContent:'center'}}>
                        <View style={styles.buttonContainer}>
                            <Text style={styles.upperText}>Pay With Paytm</Text>
                        </View>
                    
                </TouchableHighlight>
                {/* <View style={{marginTop : 50}}><Text>{ack}</Text></View> */}
                <Modal visible={showModal}
                    onRequestClose={()=>this.setState({showModal:false})}>
                    <WebView source={{ uri: 'http://192.168.1.100:3001/api/paytm/request' }} 
                        injectedJavaScript={`document.getElementById('ORDER_ID').value = "${ORDER_ID}"; document.getElementById('TXN_AMOUNT').value = "${TXN_AMOUNT}";document.getElementById('CUST_ID').value = "${CUST_ID}"; document.f1.submit(); true;`}
                        javaScriptEnabled={true}
                        onNavigationStateChange={data => this._handleResponse(data.title)}
                        />
                </Modal>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: null,
        height: null,
    },
    buttonContainer:{
        borderRadius: 5,
        backgroundColor: "black",
        width: 200,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: '#000000',
        shadowOffset: {
          width: 0,
          height: 10
        },
        shadowRadius: 10,
        shadowOpacity: 0.2,
      },
      upperText:{
        color:"white",
        fontSize: 18,
      },
});