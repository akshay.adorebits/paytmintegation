import React from 'react';
import { TouchableHighlight, Alert, View, Text, StyleSheet, Dimensions, NativeModules, ActivityIndicator, DeviceEventEmitter, TextInput , Modal} from 'react-native';
import { WebView } from 'react-native-webview';
//import Paytm from '@philly25/react-native-paytm';
var {height, width} = Dimensions.get('window');

// const paytmConfig = {
//     MID: 'OOZWnL70061838968791',
//     WEBSITE: 'WEBSTAGING',
//     CHANNEL_ID: 'WAP',
//     INDUSTRY_TYPE_ID: 'Retail',
//     CALLBACK_URL: 'https://securegw.paytm.in/theia/paytmCallback?ORDER_ID='
// };

export default class AddPayment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // amount: 0,
            // order_id: '',
            // processing: false,
            // payment_text: 'Requesting payment, please wait...'
            showModal : false,
            ack : "",
            ORDER_ID : "12345",
            TXN_AMOUNT : "500",
            CUST_ID : "123"
        };

        // this._setAmount = this._setAmount.bind(this);

        // this._startPayment = this._startPayment.bind(this);
        // this._handlePaytmResponse = this._handlePaytmResponse.bind(this);

    }

    // UNSAFE_componentWillMount() {
    //    // Paytm.addListener(Paytm.Events.PAYTM_RESPONSE, this.onPayTmResponse);
    // }
    
    // componentWillUnmount() {
    //   //  Paytm.removeListener(Paytm.Events.PAYTM_RESPONSE, this.onPayTmResponse);
    // }

    // _startPayment(){
    //     //if(this.state.processing) return;
    //     alert();
    // }
    render() {
        let { showModal, ack , ORDER_ID, TXN_AMOUNT, CUST_ID } = this.state;
        return (
            <View style={styles.container}>
                {/* <View style={{ flex: 1 }}>
                    <TextInput ref={component => this._amountTxtBox = component}
                        style={{ height: 50, backgroundColor: '#EDEDED', color: 'black', marginLeft: 20, marginRight: 20, marginTop: 20,paddingHorizontal:10,fontSize:18}}
                        onChangeText={(text) => this.setState({ amount: text })}
                        autoCorrect={false}
                        placeholder=" Enter Amount"
                        placeholderTextColor="#727278"
                        keyboardType="numeric"
                    />
                    <View style={{ width: width, flex: 1 }}>
                        {/* {(this.state.processing) ? (
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <ActivityIndicator
                                    animating={this.state.processing}
                                    style={{ height: 80 }}
                                    color="black"
                                    size="large" />
                                <Text style={{ marginTop: 5, fontSize: 15, fontWeight: 'bold' }}>{this.state.payment_text}</Text>
                            </View>
                        ) : (
                                <TouchableHighlight onPress={this._startPayment} underlayColor='transparent' style={{marginTop:50, alignItems: 'center', justifyContent: 'center' }}>
                                    <View style={styles.buttonContainer}>
                                        <Text style={styles.upperText}> Pay via PayTM </Text>
                                    </View>
                                </TouchableHighlight>
                                <Modal>

                                </Modal>
                            )} 
                            
                    </View>
                </View>
                 */}
                 {/* <TouchableHighlight onPress={this._startPayment} underlayColor='transparent' style={{marginTop:50, alignItems: 'center', justifyContent: 'center' }}>
                    <View style={styles.buttonContainer}>
                        <Text style={styles.upperText}> Pay via PayTM </Text>
                    </View>
                </TouchableHighlight>
                <Modal>
                    <WebView 
                        source={{uri : "http://192.168.1.101:3001/api/paytm/request"}}
                        injectedJavaScript={`document.getElementById('ORDER_ID').value= "${ORDER_ID}`}" ;`/>
                </Modal> */}
                <WebView source={{ uri: 'https://reactnative.dev/' }} />;
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: null,
        height: null,
    },
    buttonContainer:{
        borderRadius: 5,
        backgroundColor: "black",
        width: 200,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: '#000000',
        shadowOffset: {
          width: 0,
          height: 10
        },
        shadowRadius: 10,
        shadowOpacity: 0.2,
      },
      upperText:{
        color:"white",
        fontSize: 18,
      },
});