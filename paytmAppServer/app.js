const express = require('express');
const bodyparser = require('body-parser');
const engines = require('consolidate');
//const cores = require('cores');

const app = express();

app.engine("ejs", engines.ejs);
app.set("views","./views");
app.set("view engine", "ejs");

app.use("/api", bodyparser.urlencoded({extended:true}),bodyparser.json(),require("./routes"))
//app.use(cores());
app.use(express.static("public"));

app.get("/", (req,res)=>{
    res.send({
        message : "Its working fine"
    })
})

module.exports = app;